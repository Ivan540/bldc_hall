/*
 * uart.h
 *
 *  Created on: 6 мар. 2019 г.
 *      Author: ivan
 */

#ifndef UART_H_

#include <avr/io.h>
#include <avr/eeprom.h>
#include "ee.h"
#define UART_H_
#define TRUE 1
#define FALSE 0

//UART SPEED
#define BAUD 9600
#define UBRR_VAL F_CPU/16/BAUD-1

//UART FLAGS

/*
#ifdef  BR_9600
#define DATA_REGISTER_EMPTY (1<<UDRE)
#define RX_COMPLETE (1<<RXC)
#define FRAMING_ERROR (1<<FE)
#define PARITY_ERROR (1<<PE)
#define DATA_OVERRUN (1<<DOR)
#endif
*/
//#ifdef  BR_19200
#define DATA_REGISTER_EMPTY (1<<UDRE)
#define RX_COMPLETE (1<<RXC)
#define FRAMING_ERROR (1<<FE)
#define PARITY_ERROR (1<<UPE)
#define DATA_OVERRUN (1<<DOR)
//#endif

//UART MSG SETTINGS
//#define ADDRESS 0x07
#define SOH 0xbb
//#define RX_SIZE (RX_BUFFER_SIZE+1)
#define RX_BUFFER_SIZE 3
#define TX_BUFFER_SIZE 3

//UART bUFFERS
char rx_buffer[RX_BUFFER_SIZE];
char tx_buffer[TX_BUFFER_SIZE];
uint16_t  ADDRESS;
uint16_t  ADDRESS1;
uint16_t  ADDRESS2;
uint16_t  ADDRESS3;
uint16_t  ADDRESS4;
char SETTINGS=0;
char setting_counter=0;
int settings_limit=0;
//UART VAR
unsigned char rx_wr_index=(RX_BUFFER_SIZE);
unsigned char tx_wr_index=0;
unsigned char rx_counter;
unsigned char read_enable=0;
uint16_t number EEMEM = 0;
uint16_t number1 EEMEM = 0;
uint16_t number2 EEMEM = 0;
uint16_t number3 EEMEM = 0;
uint16_t number4 EEMEM = 0;
uint16_t protect EEMEM = 0;

void init_address()
{

	ADDRESS1=eeprom_read_word(&number1);
	ADDRESS2=eeprom_read_word(&number2);
	ADDRESS=eeprom_read_word(&number);
	ADDRESS3=eeprom_read_word(&number3);
	ADDRESS4=eeprom_read_word(&number4);



	if (ADDRESS1==ADDRESS2)
	{
		ADDRESS=ADDRESS1;
	}
	else
	{
		if (ADDRESS1==ADDRESS2)
		{
			ADDRESS=ADDRESS1;
		}
		if (ADDRESS1==ADDRESS3)
		{
			ADDRESS=ADDRESS1;
		}
		if (ADDRESS1==ADDRESS4)
		{
			ADDRESS=ADDRESS1;
		}
		if (ADDRESS2==ADDRESS3)
		{
			ADDRESS=ADDRESS2;
		}
		if (ADDRESS2==ADDRESS4)
		{
			ADDRESS=ADDRESS2;
		}
		if (ADDRESS3==ADDRESS4)
		{
			ADDRESS=ADDRESS3;
		}
	}


}

char checkaddr( char adr)
{


	if (adr==0xAA)
	{
			SETTINGS=1;
			return TRUE;
	}
		else
		{
			SETTINGS=0;
			setting_counter=0;
			if (adr==ADDRESS)
				return TRUE;
				else
				return FALSE;
		}

}

void UARTSend1(long int data) {
	DDRD|=0X02;//TXD
	UDR=data;
	while(!(UCSRA&(1<<UDRE)));

	DDRD&=0XFD;
	_delay_ms(10);
}

void usart_init (unsigned int speed)
{
	UBRRH=(unsigned char)(speed>>8);// Baud Rate
	UBRRL=(unsigned char) speed;// Baud Rate
	UCSRA=0x00;
	UCSRB|=(1<<TXEN)|(1<<RXEN)|(0<<RXCIE)|(0<<TXCIE);// Разрешение приёма и передачи
	UCSRB|=(1<<RXCIE);// Разрешение прерываний по приему
	UCSRC=(1<<UCSZ1)|(1<<UCSZ0)|(1<<URSEL);// Установка формата посылки: 8 бит данных, 1 стоп-бит
	//DDRB=0xFF;
	//DDRD=0xFF; //порт D работает как выход, так как к нему относятся выводы OC0A и OC0B
}


void setting_check()
{
	if (SETTINGS==1)
	{
		//UARTSend1(0xAA);
		if (setting_counter==0)
		{
			settings_limit=rx_buffer[2];
			setting_counter++;

		}
		else
		{
			switch(setting_counter)
			{
			case 0x01:
			{
				ADDRESS=rx_buffer[2];

				eeprom_write_word(&number1,ADDRESS);
				eeprom_write_word(&number2,ADDRESS);
				eeprom_write_word(&number3,ADDRESS);
				eeprom_write_word(&number,ADDRESS);
				eeprom_write_word(&number4,ADDRESS);
				eeprom_write_word(&protect,ADDRESS);
				 break;
			}
			case 0x02:
			{
				setting_counter=0;
				SETTINGS=0;
				 break;
			}
			}
		}
	}
}
//char checkaddr(char adr);
//void message();
//void usart_init (unsigned int speed);
//void UARTSend(unsigned char data);


#endif /* UART_H_ */
