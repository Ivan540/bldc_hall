/*
 * bldc_hall.c
 *
 *  Created on: 18 мар. 2019 г.
 *      Author: ivan
 */


#define  F_CPU 8000000L
#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/iom8.h>
#include <util/delay.h>
#include <inttypes.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <avr/eeprom.h>
#include <avr/wdt.h>
#include "uart.h"
#include <avr/wdt.h>


#define LED1 3
#define LED_PORT1 PORTD

unsigned char spd=0;
unsigned char speed=0;
unsigned char dir=2;
unsigned char cur_dir=2;
unsigned char flag=0;
unsigned char beat=0;


void message()
{
	tx_buffer[0]=SOH;
	tx_buffer[1]=ADDRESS;
	tx_buffer[2]=0xEE;


	//tx_buffer[5]=0xAA;
}

void clean()
{
	rx_buffer[0]=0x00;
		rx_buffer[1]=0x00;
		rx_buffer[2]=0x00;

}


void uart_act()

{
	LED_PORT1|=(1<<PD5);
	//wdt_reset();
	//_WDR();
//	LED_PORT1|=(1<<LED1);
//			_delay_ms(5000);
	setting_check();

	beat=0;
	spd=rx_buffer[2];
	speed=spd;

	//LED_PORT1=1<<LED1;
		if (speed&(1<<7))
		{
			spd=speed<<1;
			cur_dir=0;
		}
		else
		{
			spd=speed<<1;
			cur_dir=1;
		}


	PORTC|=(1<<5);
	UCSRB |=(1<<UDRIE);


}

ISR (TIMER2_OVF_vect)
{
beat++;
if (beat>=90)
{

	beat=0x00;
	 while (OCR1A>0)
	 {
	 OCR1A=OCR1A-0x01;
	 _delay_ms(3);
	 }
}
}

ISR (USART_TXC_vect)
{
	UCSRB&=~(1<<TXCIE);
	PORTC&= ~(1<<5);
}



ISR (USART_UDRE_vect)
{
	if(tx_wr_index == TX_BUFFER_SIZE)  	// Вывааели весь буффер?
	{
	tx_wr_index=0;
	UCSRB|=(1<<TXCIE);
	UCSRB &=~(1<<UDRIE);	// Запрещаем прерывание по опустошению - передача закончена
	flag=0;
	}
	else
	{
		if(tx_wr_index == 0)  	// Вывааели весь буффер?
				{
				UDR = SOH;
				}
		else
				UDR = tx_buffer	[tx_wr_index];
	tx_wr_index ++;	// Берем данные из буффера.
	}
}


ISR(USART_RXC_vect)
{

		char status,data;
		status=UCSRA;
		data=UDR;
		if ((status & (FRAMING_ERROR | DATA_OVERRUN))==0)
		{
			if ((data == 0xBB)&&(rx_wr_index == RX_BUFFER_SIZE)&&(read_enable == 0))
						{
				rx_wr_index=0;
				read_enable = 1;
						}
				if (read_enable == 1)
						{
							rx_buffer[rx_wr_index]=data;
							rx_wr_index++;
							if (rx_wr_index==RX_BUFFER_SIZE)
							{

								if (checkaddr(rx_buffer[1])==1)
								{
									uart_act();
								}
								read_enable = 0;

							}
						}
		}
}

void pwm_init(void)
{
    DDRB |= (1<<DDB1);
    PORTB|=(1<<PB4);
    OCR1A = 0;  // set PWM for 50% duty cycle
    TCCR1A=(1<<COM1A1)|(1<<WGM10); //На выводе OC1A единица, когда OCR1A==TCNT1, восьмибитный ШИМ
    TCCR1B=(0<<CS11)|(1<<CS10);;
}

void watchdog_init()
{
	TIMSK |= (1<<TOIE2);
	TCCR2 |= (1<<CS22)|(1<<CS20)|(1<<CS21);
}

void port_init(void)
{
DDRD&=0XFF;
DDRD=0b01101100;
DDRC|=(1<<PC1);
DDRC|=(1<<PC5);
PORTC&= ~(1<<5);
//LED_PORT1|=(1<<PD5);
LED_PORT1|=(1<<PD6);
//LED_PORT1|=(1<<PD7);
PORTC|=(1<<PC1);
LED_PORT1|=(1<<LED1);

}

void init_devices(void)
{
 sei();
 port_init();
 pwm_init();
 clean();
 watchdog_init();
 usart_init(UBRR_VAL);

 //ADDRESS=eeprom_read_word(&number);
 init_address();



// WDTCR |= (1<<WDCE) | (1<<WDE);
// WDTCR |= (1<<WDP0) | (1<<WDP1)| (1<<WDP2);

}
void direction (void)
{
	switch(cur_dir)
								 {
								 case 0x00:
								 {
									 if (dir!=0)
									 {
										// OCR1A=0x10;
									 LED_PORT1&= ~(1<<PD6);
									 while (OCR1A>0)
									 		{
									 			OCR1A=OCR1A-0x01;
									 			_delay_ms(3);
									 		}

									 }
									 LED_PORT1|= (1<<PD2);

									 LED_PORT1|=(1<<PD6);
									 dir=0;


									 break;
								 }
								 case 0x01:
								 {
									 if (dir!=1)
									 {
										// OCR1A=0x10;
									 LED_PORT1&= ~(1<<PD6);
									 while (OCR1A>0)
									 								 		{
									 								 			OCR1A=OCR1A-0x01;
									 								 			_delay_ms(3);
									 								 		}
									 }
									 LED_PORT1&= ~(1<<PD2);


									 LED_PORT1|= (1<<PD6);
									 dir=1;
								 break;
								 }
								 case 0x02:
								 {
									 LED_PORT1&= ~(1<<PD6);
									 dir=2;
								 break;
								 }
								 case 0x03:
								 {
								 LED_PORT1&= ~(1<<PD6);
								 dir=2;
								 break;
								 }

								 }
}
void run (void)
{

		while (OCR1A<spd)
		{
			OCR1A=OCR1A+0x01;
			_delay_ms(4);
		}
		while (OCR1A>spd)
		{
			OCR1A=OCR1A-0x01;
			_delay_ms(4);
		}


		flag =1;



}
int main(void)
{
	init_devices();
	sei();
	//buffer_index=0;
	//rx_wr_index = 5;
	//zf_flag=0x05;
	while(1)
	{
//		LED_PORT1|=(1<<LED1);
//		_delay_ms(50);
//		LED_PORT1&= ~(1<<LED1);
//		_delay_ms(50);
		message();
//		PORTC|=(1<<5);
//			UARTSend1(0xaa);
		if (flag==0)
		{
		direction();
		run();
		}


	}
}
