/*
 * ee.h
 *
 *  Created on: 30 янв. 2020 г.
 *      Author: rovbuilder
 */

#ifndef EE_H_
#define EE_H_

void eechange(uint16_t ee, uint16_t value)
{
	eeprom_write_word(&ee,value);
}

void eeread(uint16_t ee, uint16_t value)
{
	value=eeprom_read_word(&ee);
}


#endif /* EE_H_ */
